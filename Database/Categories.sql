﻿CREATE TABLE [dbo].[Categories] (
    [Id]   INT           IDENTITY (1, 1) NOT NULL,
    [Name] NVARCHAR (50) NOT NULL,
    CONSTRAINT [PK__Categori__3214EC073773810F] PRIMARY KEY CLUSTERED ([Id] ASC)
);


