﻿app.config([
  '$routeProvider', function ($routeProvider) {
      return $routeProvider
        .when("/Home",
        { templateUrl: "App/Views/Categories/Index.html" })
        .otherwise
        ({ redirectTo: "/Home" })
  }])