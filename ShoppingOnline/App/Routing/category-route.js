﻿app.config([
  '$routeProvider', function ($routeProvider) {
      return $routeProvider
        .when("/Categories",
          { templateUrl: "App/Views/Categories/Index.html" })
        .when("/Category/:id",
          { templateUrl: "App/Views/Categories/Detail.html" })
        .when("/Category/:id/Edit",
         { templateUrl: "App/Views/Categories/Edit.html" })
  }])